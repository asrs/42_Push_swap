/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/17 13:05:39 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/17 13:05:39 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _CHECKER_H
# define _CHECKER_H

# include "libft.h"
# include <unistd.h>
# include <stdlib.h>
# include <stdbool.h>
# include <sys/types.h>
# include <sys/uio.h>

typedef struct			s_data
{
	void				(*f[8])(struct s_data *data);
	char				**inst_name;
	int					*stka;
	int					*stkb;
	int					nb_a;
	int					nb_b;
	size_t				len;
}						t_data;

void					m_process(int ac, char **av, t_data *data);
bool					m_reader(t_data *data);

bool					ft_issort(t_data *data);
void					ft_message(int ind, t_data *data);

void					inst_sa(t_data *data);
void					inst_pa(t_data *data);
void					inst_ra(t_data *data);
void					inst_rra(t_data *data);

void					inst_sb(t_data *data);
void					inst_pb(t_data *data);
void					inst_rb(t_data *data);
void					inst_rrb(t_data *data);

#endif
