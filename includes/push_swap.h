/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/17 13:04:28 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/17 13:21:03 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _PUSH_SWAP_H
# define _PUSH_SWAP_H

# include "libft.h"
# include <unistd.h>
# include <stdlib.h>
# include <stdbool.h>

# define RANGE 12

typedef struct			s_data
{
	int					mid;
	int					per;
	int					nb_a;
	int					nb_b;
	int					*stka;
	int					*stkb;
	int					*stmp;
	int					large;
	int					small;
	int					cnt[4];
	size_t				len;
}						t_data;

int						ft_n_sort(int *tab, int n, int len);
bool					ft_issort(int *tab, int n, int len, int sens);
void					m_process(int ac, char **av, t_data *data);
void					m_algo(t_data *data);
void					al_tiny(t_data *data, int len);
void					al_select(t_data *data);
void					al_set_tmp(t_data *data);
void					al_merge_final(t_data *data);
void					ft_largest(int *tab, int *large, int n, int len);
void					ft_smallest(int *tab, int *small, int n, int len);
void					ft_message(int ind, t_data *data);
void					inst_sa(t_data *data, int print);
void					inst_pa(t_data *data, int print);
void					inst_ra(t_data *data, int print);
void					inst_rra(t_data *data, int print);
void					inst_sb(t_data *data, int print);
void					inst_pb(t_data *data, int print);
void					inst_rb(t_data *data, int print);
void					inst_rrb(t_data *data, int print);
void					inst_ss(t_data *data, int print);
void					inst_rr(t_data *data, int print);
void					inst_rrr(t_data *data, int print);

#endif
