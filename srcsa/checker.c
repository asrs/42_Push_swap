/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/17 13:06:29 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/17 13:06:29 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static inline void			init(t_data *data)
{
	data->inst_name = ft_strsplit("sa pa ra rra sb pb rb rrb ss rr rrr", ' ');
	data->nb_a = 0;
	data->nb_b = 0;
	data->len = 0;
	data->f[0] = inst_sa;
	data->f[1] = inst_pa;
	data->f[2] = inst_ra;
	data->f[3] = inst_rra;
	data->f[4] = inst_sb;
	data->f[5] = inst_pb;
	data->f[6] = inst_rb;
	data->f[7] = inst_rrb;
}

static void					checker(int ac, char **av)
{
	t_data					data;

	init(&data);
	m_process(ac, av, &data);
	m_reader(&data);
}

int							main(int ac, char **av)
{
	if (ac < 2)
		return (1);
	else
		checker(ac, av);
	return (0);
}
