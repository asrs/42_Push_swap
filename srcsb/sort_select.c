/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_select.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/17 13:07:22 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/17 13:07:22 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static bool			check_sm(t_data *data)
{
	int				i;
	int				j;
	int				n;
	int				posb;

	i = 0;
	n = 0;
	posb = ((int)(data->len) - data->nb_b);
	while (i < data->nb_b)
	{
		j = 0;
		while (j < data->per)
		{
			if ((data->cnt[0] + j) < (int)data->len && \
					data->stkb[posb + i] == data->stmp[data->cnt[1] + j])
				n++;
			j++;
		}
		if (n == data->per)
			break ;
		i++;
	}
	return ((n == data->per) ? true : false);
}

static bool			check_gt(t_data *data)
{
	int				i;
	int				j;
	int				n;
	int				posb;

	i = 0;
	n = 0;
	posb = ((int)(data->len) - data->nb_b);
	while (i < data->nb_b)
	{
		j = 0;
		while (j < data->per)
		{
			if ((data->cnt[2] + j) < (int)data->len && \
					data->stkb[posb + i] == data->stmp[data->cnt[2] + j])
				n++;
			j++;
		}
		if (n == data->per)
			break ;
		i++;
	}
	return ((n == data->per) ? true : false);
}

static void			push_it(t_data *data)
{
	int				i;
	int				posa;

	i = 0;
	while (i < data->per)
	{
		posa = ((int)(data->len) - data->nb_a);
		if (data->cnt[0] >= 0 && \
				data->stka[posa] == data->stmp[data->cnt[1] + i])
		{
			inst_pb(data, 1);
			inst_rb(data, 1);
			break ;
		}
		if ((data->cnt[2] + i) <= (int)data->len && data->nb_a > 0 &&\
				data->stka[posa] == data->stmp[data->cnt[2] + i])
		{
			inst_pb(data, 1);
			break ;
		}
		i++;
	}
	if (data->nb_a > 1)
		(i >= data->per) ? inst_ra(data, 1) : 0;
}

void				al_select(t_data *data)
{
	int				len;

	len = (int)data->len;
	while (data->nb_a > 0)
	{
		push_it(data);
		if (check_sm(data) == true)
		{
			data->cnt[0] -= data->per;
			data->cnt[1] -= data->per;
			data->cnt[0] = (data->cnt[0] < 0) ? 0 : data->cnt[0];
			data->cnt[1] = (data->cnt[1] < 0) ? 0 : data->cnt[1];
		}
		if (check_gt(data) == true)
		{
			data->cnt[2] += data->per;
			data->cnt[3] += data->per;
			data->cnt[2] = (data->cnt[2] > len) ? len : data->cnt[2];
			data->cnt[3] = (data->cnt[3] > len) ? len : data->cnt[3];
		}
	}
	return ;
}
