/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   b_inst_a.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/17 13:07:20 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/17 13:07:20 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void			inst_sa(t_data *data, int print)
{
	int			tmp;
	int			posa;

	tmp = 0;
	posa = ((int)(data->len) - data->nb_a);
	if (data->len < 2 || data->nb_a < 2)
		return ;
	tmp = data->stka[posa];
	data->stka[posa] = data->stka[posa + 1];
	data->stka[posa + 1] = tmp;
	(print == 1) ? ft_putendl("sa") : 0;
}

void			inst_pa(t_data *data, int print)
{
	int			posa;
	int			posb;

	posa = ((int)(data->len - 1) - data->nb_a);
	posb = ((int)(data->len) - data->nb_b);
	posa = (posa < 0) ? 0 : posa;
	posb = (posb < 0) ? 0 : posb;
	if (data->nb_b <= 0)
		return ;
	data->stka[posa] = data->stkb[posb];
	data->stkb[posb] = 0;
	data->nb_a++;
	data->nb_b--;
	(print == 1) ? ft_putendl("pa") : 0;
}

void			inst_ra(t_data *data, int print)
{
	int			i;
	int			tmp;
	int			posa;
	int			len;

	i = 0;
	posa = ((int)(data->len) - data->nb_a);
	len = (int)data->len - posa;
	if (data->nb_a < 1)
		return ;
	tmp = data->stka[posa];
	while (i < len)
	{
		if (i < (len - 1))
			data->stka[posa + i] = data->stka[posa + (i + 1)];
		i++;
	}
	data->stka[posa + (i - 1)] = tmp;
	(print == 1) ? ft_putendl("ra") : 0;
}

void			inst_rra(t_data *data, int print)
{
	int			i;
	int			posa;
	int			len;
	int			tmp;
	int			last;

	i = 0;
	posa = ((int)(data->len) - data->nb_a);
	len = (int)data->len - posa;
	last = posa + (len - 1);
	if (data->nb_a < 1)
		return ;
	tmp = data->stka[posa + (len - 1)];
	while (i < len)
	{
		if (i < (len - 1))
			data->stka[last - i] = data->stka[last - (i + 1)];
		i++;
	}
	data->stka[posa] = tmp;
	(print == 1) ? ft_putendl("rra") : 0;
}
