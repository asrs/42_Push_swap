/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   merge_final.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/17 13:07:21 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/17 13:07:21 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void				overall_large(t_data *data, int *large)
{
	int				i;
	int				posa;
	int				posb;

	(*large) = 0;
	posa = ((int)(data->len) - data->nb_a);
	posb = ((int)(data->len) - data->nb_b);
	i = 0;
	while (i < data->nb_a)
	{
		if ((*large) < data->stka[posa + i])
			(*large) = data->stka[posa + i];
		i++;
	}
	i = 0;
	while (i < data->nb_b)
	{
		if ((*large) < data->stkb[posb + i])
			(*large) = data->stkb[posb + i];
		i++;
	}
}

static void				overall_small(t_data *data, int *small)
{
	int				i;
	int				posa;
	int				posb;

	(*small) = data->large;
	posa = ((int)(data->len) - data->nb_a);
	posb = ((int)(data->len) - data->nb_b);
	i = 0;
	while (i < data->nb_a)
	{
		if ((*small) > data->stka[posa + i])
			(*small) = data->stka[posa + i];
		i++;
	}
	i = 0;
	while (i < data->nb_b)
	{
		if ((*small) > data->stkb[posb + i])
			(*small) = data->stkb[posb + i];
		i++;
	}
}

static void				push_it(t_data *data)
{
	int					posa;

	posa = ((int)(data->len) - data->nb_a);
	inst_pa(data, 1);
	if (data->stka[posa] > data->stka[posa + 1])
		inst_ra(data, 1);
}

void					al_merge_final(t_data *data)
{
	int				posa;
	int				posb;
	int				la;
	int				lb;

	ft_largest(data->stka, &la, data->nb_a, (int)data->len);
	ft_largest(data->stkb, &lb, data->nb_a, (int)data->len);
	overall_large(data, &data->large);
	overall_small(data, &data->small);
	while (data->nb_b > 0)
	{
		posa = ((int)(data->len) - data->nb_a);
		posb = ((int)(data->len) - data->nb_b);
		if (lb > la && data->stkb[posb] > la && data->stka[posa] == data->small)
			push_it(data);
		else if (data->stkb[posb] == data->small && data->stka[posa] == la)
			push_it(data);
		else if (data->stka[posa] > data->stkb[posb])
			push_it(data);
		else
			inst_ra(data, 1);
	}
}
