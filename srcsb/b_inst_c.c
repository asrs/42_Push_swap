/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   b_inst_c.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/17 13:07:20 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/17 13:07:20 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void			inst_ss(t_data *data, int print)
{
	inst_sa(data, print);
	inst_sb(data, print);
	ft_putendl("ss");
}

void			inst_rr(t_data *data, int print)
{
	inst_ra(data, print);
	inst_rb(data, print);
	ft_putendl("rr");
}

void			inst_rrr(t_data *data, int print)
{
	inst_rra(data, print);
	inst_rrb(data, print);
	ft_putendl("rrr");
}
