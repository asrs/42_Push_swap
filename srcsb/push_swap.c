/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/17 13:07:22 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/17 13:07:22 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static inline void			init(t_data *data)
{
	data->nb_a = 0;
	data->nb_b = 0;
	data->cnt[0] = 0;
	data->cnt[1] = 0;
	data->cnt[2] = 0;
	data->cnt[3] = 0;
	data->mid = 0;
	data->per = 0;
	data->large = 0;
	data->small = 0;
	data->len = 0;
}

static void					push_swap(int ac, char **av)
{
	t_data					data;

	init(&data);
	m_process(ac, av, &data);
	m_algo(&data);
	ft_message(0, &data);
}

int							main(int ac, char **av)
{
	if (ac < 2)
		return (1);
	else
		push_swap(ac, av);
	return (0);
}
