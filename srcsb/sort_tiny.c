/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_tiny.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/17 13:07:22 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/17 13:36:31 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

#define STK data->stka
#define MIN data->small
#define MAX data->large

static void		split(t_data *data)
{
	while (data->nb_a > 3)
		inst_pb(data, 1);
}

static void		three(t_data *data, int len)
{
	int			posa;

	posa = ((int)(data->len) - len);
	if (ft_issort(data->stka, data->nb_a, (int)data->len, 0) == true)
		return ;
	if (STK[posa] > STK[posa + 1] && STK[posa] > STK[posa + 2])
		inst_ra(data, 1);
	else if (STK[posa] < STK[posa + 1] && STK[posa + 1] > STK[posa + 2])
		inst_rra(data, 1);
	else if (STK[posa] < STK[posa + 1] && STK[posa + 1] < STK[posa + 2])
		inst_rra(data, 1);
	if (STK[posa] > STK[posa + 1] && STK[posa] < STK[posa + 2])
		inst_sa(data, 1);
}

static void		five(t_data *data)
{
	int			posb;

	if (ft_issort(data->stka, data->nb_a, (int)data->len, 0) == true)
		return ;
	split(data);
	if (ft_n_sort(data->stka, data->nb_a, (int)data->len) != data->nb_a)
		three(data, data->nb_a);
	posb = ((int)(data->len) - data->nb_b);
	if (data->len > 4 && data->stkb[posb] > data->stkb[posb + 1])
		inst_sb(data, 1);
	al_merge_final(data);
}

void			al_tiny(t_data *data, int len)
{
	int			posa;

	posa = ((int)(data->len) - len);
	if (len == 2)
	{
		if (STK[posa] > STK[posa + 1])
			inst_sa(data, 1);
	}
	if (len == 3)
		three(data, len);
	if (len < 6)
		five(data);
}
