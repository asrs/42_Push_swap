/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/26 14:34:57 by clrichar          #+#    #+#             */
/*   Updated: 2018/03/10 21:05:55 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

t_dna					*call(void)
{
	static t_dna		dna;

	return (&dna);
}

char					*unicode(wchar_t c)
{
	char				*ret;

	if (!(ret = ft_strnew(4)))
		return (NULL);
	if (c <= 0x7F)
		ret[0] = (char)c;
	else if (c <= 0x7FF && MB_CUR_MAX >= 2)
	{
		ret[0] = (char)(0xC0 | (c >> 6));
		ret[1] = (char)(0x80 | (char)(c & 0x3F));
	}
	else if (c <= 0xFFFF && MB_CUR_MAX >= 3)
	{
		ret[0] = (char)(0xE0 | (c >> 12));
		ret[1] = (char)(0x80 | ((c >> 6) & 0x3F));
		ret[2] = (char)(0x80 | (c & 0x3F));
	}
	else if (c <= 0x10FFFF && MB_CUR_MAX >= 4)
	{
		ret[0] = (char)(0xF0 | (c >> 18));
		ret[1] = (char)(0x80 | ((c >> 12) & 0x3F));
		ret[2] = (char)(0x80 | ((c >> 6) & 0x3F));
		ret[3] = (char)(0x80 | (c & 0x3F));
	}
	return (ret);
}
