/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_get.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/27 16:39:50 by clrichar          #+#    #+#             */
/*   Updated: 2018/03/10 21:05:53 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

#define FLAG dna->flag
#define MODIFER "%#0- +'"

static void		wildcard(int index, t_dna *dna)
{
	long		tmp;

	tmp = (long)va_arg(dna->va, int);
	if (tmp < 0 && (FLAG.modifier >> 5 & 1) == 1)
	{
		tmp = 0;
		FLAG.modifier &= (unsigned char)~(1 << 5);
	}
	if (index == 1 && FLAG.v_size == 0)
		FLAG.v_size = tmp;
	else if (index == 1 && FLAG.v_size != 0)
		index += 1;
	if (index == 2 && FLAG.v_padding == 0)
	{
		FLAG.v_padding = tmp;
	}
}

static void		stock_size(char *ret, int lock, t_dna *dna)
{
	if (*ret == '*' && lock == 0)
		wildcard(1, dna);
	else
		FLAG.v_size = (long)ft_atoi(ret);
	if (FLAG.v_size < 0)
	{
		FLAG.v_size = -FLAG.v_size;
		FLAG.modifier |= 4;
	}
}

static void		stock_padding(char *ret, int lock, t_dna *dna)
{
	if (ret && ft_strnchr(ret, '*') > 0 && lock == 0)
		wildcard(2, dna);
	else
	{
		while (*ret && *ret != '.')
			ret++;
		ret++;
		FLAG.v_padding = (long)ft_atoi(ret);
	}
	if (FLAG.v_padding < 0)
	{
		FLAG.v_padding = -FLAG.v_padding;
		FLAG.modifier |= 4;
	}
}

void			get_modifier(char *s, t_dna *dna)
{
	int			i;

	i = 0;
	while (s[i])
	{
		if (s[i] == '#')
			FLAG.modifier |= 1;
		else if (s[i] == '0' && (i > 0 && (!(ft_isdigit(s[i - 1]) || \
							s[i - 1] == '.'))))
			FLAG.modifier |= 2;
		else if (s[i] == '-')
			FLAG.modifier |= 4;
		else if (s[i] == ' ')
			FLAG.modifier |= 8;
		else if (s[i] == '+')
			FLAG.modifier |= 16;
		else if (s[i] == '.')
			FLAG.modifier |= 32;
		i++;
	}
}

void			get_precision(char *s, t_dna *dna)
{
	int			i;
	int			lock;
	char		*ret;

	i = 0;
	lock = 0;
	ret = NULL;
	if (ft_strnchr(s, 'p'))
		lock = 1;
	while (*s && ft_strnchr(MODIFER, *s) == 1)
		s++;
	while (s[i] && (ft_isdigit(s[i]) == 1 || s[i] == '.' || s[i] == '*'))
		i++;
	ret = ft_strsub(s, 0, (size_t)i);
	stock_size(ret, lock, dna);
	if (ft_strnchr(ret, '.') > 0 || ft_strnchr(ret, '*') > 1)
		stock_padding(ret, lock, dna);
	if (ret)
		ft_strdel(&ret);
}
