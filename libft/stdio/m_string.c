/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_string.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 14:04:03 by clrichar          #+#    #+#             */
/*   Updated: 2018/03/10 21:06:02 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

#define FLAG dna->flag

static void		simple_string(void)
{
	char		*ret;
	t_dna		*dna;

	dna = call();
	ret = va_arg(dna->va, char *);
	if (((FLAG.modifier >> 5 & 1) == 1) && FLAG.v_padding <= 0)
		FLAG.s_flag = ft_strdup("");
	else if (ret && (FLAG.v_padding > 0 && \
				FLAG.v_padding < (long)ft_strlen(ret)))
		FLAG.s_flag = ft_strsub(ret, 0, (size_t)FLAG.v_padding);
	else if (ret)
		FLAG.s_flag = ft_strdup(ret);
	else if (ret == NULL)
		FLAG.s_flag = ft_strdup("(null)");
	FLAG.len += (long)ft_strlen(FLAG.s_flag);
	FLAG.v_padding = 0;
}

static void		cut_wstring(wchar_t *tmp, char **ret, t_dna *dna)
{
	int			i;
	char		*s;

	i = 0;
	s = NULL;
	while (tmp[i] && ((FLAG.modifier >> 5 & 1) == 1 && FLAG.v_padding > 0))
	{
		s = unicode(tmp[i]);
		if (FLAG.v_padding >= (long)ft_strlen(s))
			*ret = ft_strjoin_free(*ret, s, 1);
		else
			break ;
		if ((FLAG.modifier >> 5 & 1) == 1)
			FLAG.v_padding -= (long)ft_strlen(s);
		(s) ? ft_strdel(&s) : 0;
		i++;
	}
	(s) ? ft_strdel(&s) : 0;
	FLAG.s_flag = ft_strdup(*ret);
}

static void		get_wstring(wchar_t *tmp, char **ret, t_dna *dna)
{
	int			i;

	i = 0;
	while (tmp[i])
	{
		*ret = ft_strjoin_free(*ret, unicode(tmp[i]), 3);
		i++;
	}
	FLAG.s_flag = ft_strdup(*ret);
}

static void		wide_string(void)
{
	wchar_t		*tmp;
	char		*ret;
	t_dna		*dna;

	dna = call();
	ret = ft_strdup("");
	tmp = va_arg(dna->va, wchar_t *);
	if (!tmp)
		FLAG.s_flag = ft_strdup("(null)");
	else if (((FLAG.modifier >> 5 & 1) == 1) && FLAG.v_padding <= 0)
		FLAG.s_flag = ft_strdup("");
	else if (((FLAG.modifier >> 5 & 1) == 1) && FLAG.v_padding > 0)
		cut_wstring(tmp, &ret, dna);
	else if (tmp && (FLAG.modifier >> 5 & 1) == 0)
		get_wstring(tmp, &ret, dna);
	(ret) ? ft_strdel(&ret) : 0;
	FLAG.len += (FLAG.s_flag) ? (long)ft_strlen(FLAG.s_flag) : 0;
	FLAG.v_padding = 0;
}

bool			ex_string(char *s)
{
	char		c;
	t_dna		*dna;

	c = s[ft_strlen(s) - 1];
	dna = call();
	if (c == 's' && ft_strnchr(s, 'l') == 0)
		simple_string();
	else if (c == 'S')
		wide_string();
	else if (c == 's' && ft_strnchr(s, 'l') == 1)
		wide_string();
	if ((FLAG.modifier >> 1 & 1) == 0)
		FLAG.v_padding = 0;
	else if ((FLAG.modifier >> 1 & 1) == 1)
	{
		FLAG.v_padding += FLAG.v_size;
		FLAG.v_size = 0;
	}
	return (true);
}
