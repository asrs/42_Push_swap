/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_color.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/26 14:59:09 by clrichar          #+#    #+#             */
/*   Updated: 2018/03/10 21:05:51 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static bool		seek(const char *s)
{
	char		*ret;

	ret = NULL;
	if (ft_strequ("{RED}", s))
		ret = ft_strdup(COLOR_RED);
	else if (ft_strequ("{GREEN}", s))
		ret = ft_strdup(COLOR_GREEN);
	else if (ft_strequ("{YELLOW}", s))
		ret = ft_strdup(COLOR_YELLOW);
	else if (ft_strequ("{BLUE}", s))
		ret = ft_strdup(COLOR_BLUE);
	else if (ft_strequ("{MAGENTA}", s))
		ret = ft_strdup(COLOR_MAGENTA);
	else if (ft_strequ("{CYAN}", s))
		ret = ft_strdup(COLOR_CYAN);
	else if (ft_strequ("{RESET}", s) || ft_strequ("{EOC}", s))
		ret = ft_strdup(COLOR_RESET);
	if (ret)
	{
		ft_buffer(ret, ft_strlen(ret));
		ft_strdel(&ret);
		return (true);
	}
	return (false);
}

bool			ft_color(const char *s)
{
	char		*ret;

	ret = NULL;
	if (seek(s) == false)
	{
		ret = ft_strdup("{");
		ft_buffer(ret, ft_strlen(ret));
		ft_strdel(&ret);
		return (false);
	}
	return (true);
}
